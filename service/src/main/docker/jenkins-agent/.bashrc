# .bashrc

# Source global definitions
if [ -f /etc/bashrc ]; then
	. /etc/bashrc
fi


export JAVA_HOME="/usr/java/latest"
export GRADLE_HOME=/opt/gradle/gradle-3.4.1
export PATH=$PATH:/opt/gradle/gradle-3.4.1/bin