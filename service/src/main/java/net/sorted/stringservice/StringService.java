package net.sorted.stringservice;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;


@SpringBootApplication
@PropertySources({ @PropertySource(value = "classpath:application.properties"),
@PropertySource(value = "file:./config/application.properties", ignoreResourceNotFound = true) })
public class StringService {

    private static Logger log = LogManager.getLogger(StringService.class);

    public static void main(String[] args) {
        log.debug("Initializing the reverser application");
        try {
            SpringApplication.run(StringService.class, args);
            log.debug("string service is up and running");
        } catch (Throwable t) {
            log.error("Failed to start reverser", t);
            System.exit(1);
        }
    }
}




