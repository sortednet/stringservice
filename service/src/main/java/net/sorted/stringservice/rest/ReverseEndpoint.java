package net.sorted.stringservice.rest;


import net.sorted.stringservice.StringReverserService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping(produces = MediaType.APPLICATION_JSON_VALUE)
public class ReverseEndpoint {

    private Logger log = LogManager.getLogger(ReverseEndpoint.class);

    @Autowired
    private StringReverserService stringReverserService;

    @RequestMapping(value="/api/reverse", method = {RequestMethod.GET})
    public Reversed reverse(String input) {
        log.debug("Reversing input {}", input);
        return new Reversed(stringReverserService.reverse(input), input);
    }

}
