package net.sorted.stringservice.rest;

public class Reversed {
    private String original;
    private String reversed;


    public Reversed(String reversed, String original) {
        this.reversed = reversed;
        this.original = original;
    }

    public String getReversed() {
        return reversed;
    }

    public String getOriginal() {
        return original;
    }
}
