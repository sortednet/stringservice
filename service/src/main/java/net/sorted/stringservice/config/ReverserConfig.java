package net.sorted.stringservice.config;


import net.sorted.stringservice.StringReverserService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ReverserConfig {

    @Bean
    public StringReverserService stringReverserService() {
        return new StringReverserService();
    }
}
