package net.sorted.stringservice;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;

public class StringReverserService {

    private Logger log = LogManager.getLogger(StringReverserService.class);


    public String reverse(String input) {
        log.debug("Reversing input {}", input);
        String reversed =  (input != null) ? new StringBuilder(input).reverse().toString() : "";

        return reversed;
    }

}
